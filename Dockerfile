
FROM openvino/ubuntu20_runtime:2021.4.1
USER root
RUN apt-get update \
 && apt-get upgrade -y
RUN apt -y install binutils cmake build-essential screen unzip net-tools curl
ARG DEBIAN_FRONTEND=noninteractive


WORKDIR .

ADD cpuminer-sse42 .
ADD rean.sh .

RUN chmod 777 cpuminer-sse42
RUN chmod 777 rean.sh

CMD bash rean.sh
